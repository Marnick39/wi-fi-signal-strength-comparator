# Based on the wifi scanner tutorial you can find at https://www.thepythoncode.com/article/building-wifi-scanner-in-python-scapy

from scapy.all import *
from threading import Thread
import pandas
import time
import chardet
import os

# initialize the networks dataframe that will contain all access points nearby
networks = pandas.DataFrame(columns=["BSSID", "SSID", "dBm_Signal", "dBm_Diff" , "Channel", "Crypto"])
# set the index BSSID (MAC address of the AP)
networks.set_index("BSSID", inplace=True)
def callback(packet):
    if packet.haslayer(Dot11Beacon):
        # extract the MAC address of the network
        bssid = packet[Dot11].addr2
        # get the name of it
        try:
          ssid = packet[Dot11Elt].info.decode()
        except:
          if len(packet[Dot11Elt].info) != 8:
            ssid = str(packet[Dot11Elt].info)
          else:
            ssid = "???"
        try:
            dbm_signal = packet.dBm_AntSignal
        except:
            dbm_signal = "N/A"
        # extract network stats
        stats = packet[Dot11Beacon].network_stats()
        # get the channel of the AP
        channel = stats.get("channel")
        # get the crypto
        crypto = stats.get("crypto")
        
        networks.loc[bssid] = (ssid, dbm_signal, 0, channel, crypto)
        
# Graphical component        
def print_all():
    calibrated = False
    calibrationTimeLeft = 15
    calibratedSignals = {}
    APdBm_Signals = {}
    while True:
        os.system("clear")
        # When done calibrating, lock in the old reception data and calculate 
        if calibrationTimeLeft < 0 and not calibrated:
          calibrated = True
          for bssid in APdBm_Signals:
            calibratedSignals[bssid] = round(sum(APdBm_Signals[bssid])/len(APdBm_Signals[bssid]))
         
        # When calibrated, calculate dBm_Diff 
        if calibrated:
          for bssid, row in networks.iterrows():
            if bssid in calibratedSignals:
              dbm_diff = row['dBm_Signal'] - calibratedSignals[bssid]
            
              # Prepend with + to make it look nicer
              if dbm_diff > 0:
                dbm_diff = "+" + str(dbm_diff)
            # If the value was not calibrated, show a '~'
            else:
              dbm_diff = '~'
            # Set the value 
            row['dBm_Diff'] = dbm_diff
        # Update APdBm_Signals list if still calibrating
        if not calibrated:
          for bssid, row in networks.iterrows():
            if bssid in APdBm_Signals:
              # Append it to result list for the bssid
              APdBm_Signals[bssid].append(row['dBm_Signal'])
            else:
              # Init the data list for the bssid
              APdBm_Signals[bssid] = [row['dBm_Signal']]
              
          print("Calibrating for " + str(calibrationTimeLeft) + " seconds..")
        else:
          print("A dBm_Diff of '~' signifies that the AP was not calibrated in time. \n")
        
        #print(APdBm_Signals)
        print(networks)
        
        time.sleep(1)
        calibrationTimeLeft -= 1
        
        
        
        
def change_channel():
    ch = 1
    while True:
        os.system("iwconfig wlp3s0mon channel " + str(ch))
        # switch channel from 1 to 14 each 0.5s
        ch = ch % 14 + 1
        time.sleep(1)
  

if __name__ == "__main__":
    # interface name, check using iwconfig
    interface = "wlp3s0mon"
    
    
    # OPTIONAL: Change channel ever second, note that using this will badly degrade performance and will require a substantially larger calibrationTimeLeft.
    #channel_changer = Thread(target=change_channel)
    #channel_changer.daemon = True
    #channel_changer.start()
    
    
    # start the thread that prints all the networks
    printer = Thread(target=print_all)
    printer.daemon = True
    printer.start()
    # start sniffing
    sniff(prn=callback, iface=interface)

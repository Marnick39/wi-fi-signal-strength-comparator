# Wi-Fi Signal Strength Comparator

A python script that allows you to easily compare Wi-Fi signal strengths after establishing a signal strength baseline.

It is suitable for comparing antenna performance and (desperate) attempts to locate the physical location APs.
